scroll();

window.onscroll = function() {scroll()};

document.querySelector('.scrollToTop').addEventListener( 'click', scrollToTop );

function scrollToTop() {

    window.scrollTo({top: 0, behavior: 'smooth'});

}

function scroll() {
    if (
        document.body.scrollTop > 0 ||
        document.documentElement.scrollTop > 0
    ) {

        document.querySelector('.navBar').style.top = '0rem';
        document.querySelector('.scrollToTop').style.opacity = '1';

    } else if (
        document.body.scrollTop == 0 ||
        document.documentElement.scrollTop == 0
    ) {

        document.querySelector('.navBar').style.top = '-3rem';
        document.querySelector('.scrollToTop').style.opacity = '0';

    }
}
